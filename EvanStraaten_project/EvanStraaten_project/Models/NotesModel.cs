﻿using System;
using SQLite;
using System.Windows.Input;
using Xamarin.Forms;
using System.ComponentModel;

namespace EvanStraaten_project.Models
{
    public class NotesModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
      
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID
        {
            get;
            set;
        }
        public string name;
        public string Name
        {
          
            get { return name;  }
            set {
                name = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name")); }
        }
        public string message;
        public string Message
        {
           
            get { return message; }
            set
            {
                message = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Message"));
            }
        }
        public string theme;
        public string Theme
        {
           
            get { return theme; }
           
            set
            {
                theme = value;
            
            }
        }

        public DateTime Date
        {
            get;
            set;
        }
       
        
    }
}
