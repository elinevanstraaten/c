﻿
using System.Collections.Generic;
using SQLite;
using EvanStraaten_project.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace EvanStraaten_project.DataBase
{
    public class NotesDataBase
    {
        readonly SQLiteAsyncConnection database;
        public NotesDataBase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<NotesModel>().Wait();
        }

        public Task<List<NotesModel>> GetNotes()
        {
            return database.Table<NotesModel>().ToListAsync();
        }


        public Task<NotesModel> GetNote(int id)
        {
            return database.Table<NotesModel>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveNote(NotesModel note)
        {
            if (note.ID != 0)
            {
                return database.UpdateAsync(note);
            }
            else
            {
                return database.InsertAsync(note);
            }
        }

        public Task<int> DeleteNote(NotesModel note)
        {
            return database.DeleteAsync(note);
        }
    }
}
