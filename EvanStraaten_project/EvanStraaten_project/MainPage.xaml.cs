﻿using EvanStraaten_project.Pages;
using EvanStraaten_project.Views;
using EvanStraaten_project.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using EvanStraaten_project.Models;

namespace EvanStraaten_project
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var model = new NotesModel();
            noteslist.ItemsSource = await App.Database.GetNotes();
       
            

        
            
            //listLayout.setBackgroundColor = Color.FromHex("#ffd600");





        }
            private async void newNotes_OnClicked(object sender, EventArgs e)
        {
           

            await Navigation.PushModalAsync(new NewNote());
        }
     
        async public void OpenDetail(object sender, SelectedItemChangedEventArgs e)
        {
            await Navigation.PushModalAsync(new NoteDetail
            {
                BindingContext = e.SelectedItem as NotesModel
            });

        }

    }
    
}
