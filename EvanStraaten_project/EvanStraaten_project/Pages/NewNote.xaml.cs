﻿using EvanStraaten_project.Models;

using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace EvanStraaten_project.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewNote : ContentPage
    {
        public NotesModel notesmodel;
        
        public NewNote()
        {
            InitializeComponent();
            notesmodel = new NotesModel();
            this.BindingContext = notesmodel;
        }

        async void SaveNote(object sender, EventArgs e)
        {
            var note = (NotesModel)BindingContext;
            note.Date = DateTime.Now;
            if (PickerLabel.Text == "Important")
            {
                note.Theme = "#14587C";

            }
            if (PickerLabel.Text == "Work Related")
            {
                note.Theme = "#7EB2DD";
                
                
            }
            if (PickerLabel.Text == "Private")
            {
                note.Theme = "#467A99 ";
                

            }
            if (PickerLabel.Text == "Basic Reminder")
            {
                note.Theme = "#769FB6";
               
            }
            if (String.IsNullOrEmpty(NameEntry.Text) || String.IsNullOrEmpty(MessageEntry.Text))
            {
                 await DisplayAlert("Alert", "Please fill both fields", "OK");
            }
            else
            {
                await App.Database.SaveNote(note);
                await Navigation.PushModalAsync(new MainPage());
            }
        }
        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                PickerLabel.Text = (string)picker.ItemsSource[selectedIndex];
            }
        }
    }
}