﻿using System;
using EvanStraaten_project.DataBase;
using EvanStraaten_project.Views;
using EvanStraaten_project.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EvanStraaten_project.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoteDetail : ContentPage
    {
        public NoteDetail()
        {
            InitializeComponent();
        }
        async void EditNote(object sender, EventArgs e)
        {
            var note = (NotesModel)BindingContext;
            if (PickerLabel.Text == "Important")
            {
                note.Theme = "#14587C";

            }
            if (PickerLabel.Text == "Work Related")
            {
                note.Theme = "#7EB2DD";


            }
            if (PickerLabel.Text == "Private")
            {
                note.Theme = "#467A99 ";


            }
            if (PickerLabel.Text == "Basic Reminder")
            {
                note.Theme = "#769FB6";

            }
            await App.Database.SaveNote(note);
            await Navigation.PushModalAsync(new MainPage());
           
        }
        async void DeleteNote(object sender, EventArgs e)
        {
            var note = (NotesModel)BindingContext;
            await App.Database.DeleteNote(note);
            await Navigation.PushModalAsync(new MainPage());
        }
        
    }
}